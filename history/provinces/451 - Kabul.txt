#451 - Kabulistan

owner = A74
controller = A74
add_core = A74
culture = nathalairey
religion = regent_court

hre = no

base_tax = 13
base_production = 13
base_manpower = 13

trade_goods = slaves

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari
