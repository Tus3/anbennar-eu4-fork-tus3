#189 - Lothringen | 

owner = A27
controller = A27
add_core = A27
add_core = A28
culture = greenscale_kobold
religion = kobold_dragon_cult

hre = no

base_tax = 1
base_production = 1
base_manpower = 3

trade_goods = copper

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold

