#172 - Vendee | 

owner = A26
controller = A26
culture = redscale_kobold
religion = kobold_dragon_cult

hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = fish

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold

477.1.1 = {	
	owner = A26
	controller = A26
	add_core = A26
}