#169 - Armor | Nimscodd

owner = A06
controller = A06
add_core = A06
culture = cliff_gnome
religion = the_thought

hre = no

base_tax = 10
base_production = 10
base_manpower = 10

trade_goods = glass
capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold