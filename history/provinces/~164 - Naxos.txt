#164 - Naxos | 

owner = A01
controller = A01
add_core = A01
add_core = A69
culture = redfoot_halfling
religion = regent_court

hre = no

base_tax = 6
base_production = 6
base_manpower = 4

trade_goods = cloth
capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold
