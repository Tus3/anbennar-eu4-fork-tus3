# No previous file for Cuzco

owner = B13
controller = B13
add_core = B13
culture = vernman
religion = regent_court

hre = no

base_tax = 2
base_production = 1
base_manpower = 1

trade_goods = grain

capital = ""

is_city = yes

native_size = 0
native_ferocity = 0
native_hostileness = 0