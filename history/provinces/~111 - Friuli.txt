#111 - Friuli

owner = A02
controller = A02
add_core = A02
culture = derannic
religion = regent_court
hre = no 
base_tax = 6
base_production = 6    
trade_goods = livestock
base_manpower = 4  
capital = "" 
is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

