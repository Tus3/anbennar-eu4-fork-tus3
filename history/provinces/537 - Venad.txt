#537 - Medirudu (coppermine)

owner = F15
controller = F15
add_core = F15
culture = exodus_goblin
religion = goblinic_shamanism


base_tax = 3
base_production = 3
base_manpower = 2

trade_goods = copper

capital = ""

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari

add_permanent_province_modifier = {
	name = human_minority_integrated_small
	duration = -1
}

