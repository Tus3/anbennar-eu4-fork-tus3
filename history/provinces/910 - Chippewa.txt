#910 - Chippewa

owner = A30
controller = A30
#add_core = A30
culture = east_damerian
religion = regent_court

hre = yes

base_tax = 5
base_production = 4
base_manpower = 2

trade_goods = grain

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish

1443.11.11 = {	
	owner = A30
	controller = A30
}