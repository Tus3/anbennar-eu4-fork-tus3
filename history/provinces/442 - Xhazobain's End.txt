# No previous file for Xhazobain's End
culture = flamemarked_gnoll
religion = xhazobkult
capital = ""

hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = unknown

native_size = 3
native_ferocity = 7
native_hostileness = 10

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari