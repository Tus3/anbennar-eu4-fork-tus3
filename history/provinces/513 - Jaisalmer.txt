#513 - Quintenbury

owner = F16
controller = F16
add_core = F16
culture = ourdi
religion = regent_court


base_tax = 2
base_production = 4
base_manpower = 3

trade_goods = salt

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari