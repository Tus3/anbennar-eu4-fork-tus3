#348 - Sus | 

owner = A13
controller = A13
add_core = A13
add_core = A70
culture = bluefoot_halfling
religion = regent_court

hre = no

base_tax = 6
base_production = 4
base_manpower = 2

trade_goods = cloth

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold

