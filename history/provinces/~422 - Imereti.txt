# No previous file for Imereti
owner = A77
controller = A77
add_core = A77
culture = aldresian
religion = regent_court

hre = yes

base_tax = 3
base_production = 3
base_manpower = 5

trade_goods = wool

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish