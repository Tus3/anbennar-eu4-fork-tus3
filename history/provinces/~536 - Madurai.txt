#536 - Aqatbahar

owner = F15
controller = F15
add_core = F15
add_core = F19
culture = bahari
religion = bulwari_sun_cult

base_tax = 11
base_production = 10
base_manpower = 7

trade_goods = glass

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari