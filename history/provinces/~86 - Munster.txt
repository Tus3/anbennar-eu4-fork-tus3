#86 - Munster | 

owner = A16
controller = A16
add_core = A16
culture = sorncosti
religion = regent_court

hre = no

base_tax = 5
base_production = 4
base_manpower = 2

trade_goods = glass #Main source of wine bottles

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish