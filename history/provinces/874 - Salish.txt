# No previous file for Salish

owner = B55
controller = B55
add_core = B55
culture = castellyrian
religion = regent_court

hre = no

base_tax = 4
base_production = 4
base_manpower = 4

trade_goods = grain

capital = ""

is_city = yes
fort_15th = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish