#135 - Sopron (Sopron + Vas + Moson + Zala)

owner = A01
controller = A01
add_core = A01
add_core = A68
culture = redfoot_halfling
religion = regent_court
hre = no
base_tax = 2
base_production = 3
trade_goods = grain
base_manpower = 3
capital = "" 
is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold