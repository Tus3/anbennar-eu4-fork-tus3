# No previous file for Central Doab

owner = A85
controller = A85
add_core = A85
culture = east_damerian
religion = regent_court

hre = yes

base_tax = 3
base_production = 3
base_manpower = 1

trade_goods = glass

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish