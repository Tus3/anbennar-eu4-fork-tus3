#58 - Anhalt | Daromfort

owner = A02
controller = A02
add_core = A02
culture = low_lorentish
religion = regent_court

hre = no

base_tax = 5
base_production = 5
base_manpower = 3

trade_goods = livestock

capital = "Daromas's Fort"

is_city = yes
fort_15th = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
