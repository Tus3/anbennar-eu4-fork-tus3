#463 - Circassia

owner = F05
controller = F05
add_core = F05
add_core = F04
add_core = F01
culture = kheteratan
religion = khetist

hre = no

base_tax = 5
base_production = 3
base_manpower = 4

trade_goods = grain

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy

add_permanent_province_modifier = {
	name = mothers_sorrow_estuary_modifier
	duration = -1
}

add_permanent_province_modifier = {
	name = center_of_trade_modifier
	duration = -1
}