#157 - Bihar | 

owner = A40
controller = A40
add_core = A40
culture = imperial_halfling
religion = regent_court

hre = yes

base_tax = 5
base_production = 5
base_manpower = 7

trade_goods = cloth
capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold


