# - Graaff-Reinet

culture = green_orc
religion = great_dookan
capital = ""

hre = no

base_tax = 5
base_production = 5
base_manpower = 4

trade_goods = slaves

capital = ""

is_city = yes

native_size = 200
native_ferocity = 8
native_hostileness = 9


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
