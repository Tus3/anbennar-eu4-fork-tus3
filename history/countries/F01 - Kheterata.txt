government = despotic_monarchy
government_rank = 2
primary_culture = kheteratan
religion = khetist
technology_group = tech_salahadesi
national_focus = DIP
capital = 475

1444.1.1 = {
	monarch = {
		name = "Semut III"
		dynasty = "Crodamos"
		birth_date = 1421.2.2
		adm = 3
		dip = 3
		mil = 3
	}
}