government = merchant_republic
government_rank = 1
primary_culture = redfoot_halfling
religion = regent_court
technology_group = tech_cannorian
national_focus = DIP
capital = 63
fixed_capital = 63 # Cannot move capital away from this province & no power cost to move to it

1440.2.2 = {
	monarch = {
		name = "Arnol the Fat"
		birth_date = 1399.12.12
		adm = 3
		dip = 1
		mil = 1
	}
}