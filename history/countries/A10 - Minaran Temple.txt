government = theocratic_government
government_rank = 1
primary_culture = high_lorentish
religion = regent_court
technology_group = tech_cannorian
historical_friend = A09 #Sorncost
capital = 97


1444.1.1 = {
	monarch = {
		name = "Alessa of Roilsard"
		birth_date = 1426.1.1
		adm = 4
		dip = 4
		mil = 4
		female = yes
	}
}