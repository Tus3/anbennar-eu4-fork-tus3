government = theocratic_government
government_rank = 1
primary_culture = kheteratan
religion = khetist
technology_group = tech_salahadesi
national_focus = DIP
capital = 473

1444.1.1 = {
	monarch = {
		name = "Nahab the Defiant"
		birth_date = 1413.7.3
		adm = 3
		dip = 3
		mil = 3
	}
}