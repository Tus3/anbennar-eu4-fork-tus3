#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 255  255  255 }

revolutionary_colors = { 255  255  255 }

historical_idea_groups = {
	exploration_ideas
	maritime_ideas
	economic_ideas
	offensive_ideas
	expansion_ideas
	quality_ideas	
	administrative_ideas	
	influence_ideas
}

historical_units = {
	cannorian_1_medieval_infantry
	cannorian_1_medieval_knights
	cannorian_5_men_at_arms
	cannorian_9_pike_and_shot
	cannorian_10_gun_knights
	cannorian_12_thorn_formation
	cannorian_14_outriders
	cannorian_15_volley_infantry
	cannorian_18_reformed_outriders
	cannorian_19_thorn_formation2
	cannorian_23_line_infantry
	cannorian_26_carabiners
	cannorian_26_rosecoat_infantry
	cannorian_28_impulse_infantry
	cannorian_30_cuirassier
	cannorian_30_drill_infantry
}

monarch_names = {
	"Welyam #3" = 90
	"Willam #0" = 70
	"James #3" = 60
	"Edmund #3" = 100
	"Colyn #2" = 40
	"Toman #1" = 40
	"Humbert #3" = 80
	"Godrac #12" = 3
	"Godryc #3" = 10
	"Godric #0" = 70
	"Stovac #1" = 5
	"Ricard #1" = 55
	"Alenn #3" = 55
	"Ulric #3" = 5
	"Carlan #2" = 20
	
	
	"Acromar #0" = 1
	"Adelar #0" = 1
	"Alain #0" = 1
	"Alen #0" = 1
	"Arnold #0" = 1
	"Camir #0" = 1
	"Camor #0" = 1
	"Canrec #0" = 1
	"Celgal #0" = 1
	"Ciramod #0" = 1
	"Clarimond #0" = 1
	"Clothar #0" = 1
	"Coreg #0" = 1
	"Crovan #0" = 1
	"Crovis #0" = 1
	"Corac #0" = 1
	"Corric #0" = 1
	"Dalyon #0" = 1
	"Delia #0" = 1
	"Delian #0" = 1
	"Devac #0" = 1
	"Devan #0" = 1
	"Dustyn #0" = 1
	"Elecast #0" = 1
	"Frederic #0" = 1
	"Godwin #0" = 1
	"Gracos #0" = 1
	"Henric #0" = 1
	"Humac #0" = 1
	"Humban #0" = 1
	"Humbar #0" = 1
	"Jacob #0" = 1
	"Lain #0" = 1
	"Lan #0" = 1
	"Madalac #0" = 1
	"Marcan #0" = 1
	"Ottrac #0" = 1
	"Ottran #0" = 1
	"Petran #0" = 1
	"Peyter #0" = 1
	"Rabac #0" = 1
	"Rabard #0" = 1
	"Rycan #0" = 1
	"Rican #0" = 1
	"Rogec #0" = 1
	"Roger #0" = 1
	"Stovan #0" = 1
	"Teagan #0" = 1
	"Tomac #0" = 1
	"Tomar #0" = 1
	"Tormac #0" = 1
	"Venac #0" = 1
	"Vencan #0" = 1
	"Walter #0" = 1
	"Wystan #0" = 1
	"Bellac #0" = 1
	

	"Adela #0" = -10
	"Alice #0" = -10
	"Anna #0" = -10
	"Anne #0" = -10
	"Auci #0" = -10
	"Bella #0" = -10
	"Catherine #0" = -10
	"Clarimonde #0" = -10
	"Clarya #0" = -10
	"Clothilde #0" = -10
	"Constance #0" = -10
	"Cora #0" = -10
	"Coralinde #0" = -10
	"Dalya #0" = -10
	"Edith #0" = -10
	"Eleanor #0" = -10
	"Elenor #0" = -10
	"Emma #0" = -10
	"Etta #0" = -10
	"Humba #0" = -10
	"Lisban #0" = -10
	"Lisbet #0" = -10
	"Madala #0" = -10
	"Magda #0" = -10
	"Matilda #0" = -10
	"Robyn #0" = -10
}

leader_names = {
	Gerwick "of Vanbury" Eaglecrest Fludd Lodan Feycombe Wightsgate Humbercroft Ginnfield Derwing Alainson Cottersea Leapdon Brontay Beron Cockerwall Edgemoor Fouler Lonevalley Greenley "of Alenath" 
	Norleigh "of Morban" Jonsway Mossford Oudesker Wolfden Aldtempel Gardfort Drakesford Somberwold Westfield Baldfather Alenfield "of the Greatmarch"
	
	"of Northburn" "of Elderhollow" "of Bluesfield" Redgrave Morton Winters Brooks Robinson Alenson Woods Oldfort Newfort Eaglefort Horsefort Gilmore Swyft Mottram Rackham Thaxton Barton Bateman
	Curran Doe Elk Avery "of Ashby" "of Vineroot Hollow" Smith Hammerford Croxford Shoebridge Slowbridge Guardbridge Butler Broadhurst Mason "s�l Wex" "of Vinerick" Moss Moore Reed Wicknman Potts Toadswell Wood Townsend
}

ship_names = {
	#Generic Cannorian
	Adamant Advantage Adventure Advice Answer Ardent Armada Arrogant Assistance Association Assurance Atlas Audacious 
	Bear Beaver "Black Galley" "Black Pinnace" "Black Prince" "Black Blood" Captain Centurion Coronation Courage Crocodile Crown
	Daisy Defence Defiance Devastation Diamond Director Dolphin Dragon Drake Dreadnaught Duke Duchess Eagle Elephant Excellent Exchange Expedition Experiment
	Falcon Fame Favourite Fellowship Fleetwood Flight Flirt Formidable Forrester Fortitude Fortune Gillyflower Globe "Golden Horse" "Golden Lion" "Golden Phoenix" Goliath Goodgrace Governor
	"Grand Mistress" "Great Bark" "Great Charity" "Great Galley" "Great Gift"  "Great Pinnace" "Great Zebra" "Green Dragon" Greyhound Griffin Guide "Half Moon" Hare Harpy Hawke Hazardous Heart
	Hero Hope "Hope Bark" "Hopeful Adventure" Humble Hunter Illustrious Impregnable Increase Indefatigable Inflexible Intrepid Invincible "Less Bark" "Less Pinnace" "Less Zebra"
	Lively Lizard Lion Magnanime Magnificent Majestic Makeshift Medusa Minotaur Moderate Monarch Moon Moor "New Bark" Ocean 
	Pansy Panther Parrot Porcupine Powerful President Prince "Prince Consort" "Prince Royal"
	Redoubtable Reformation Regent Renown Repulse Research Reserve Resistance Resolution Restoration Revenge 
	Salamander Sandwich Sapphire Satisfaction Seahorse Search Sheerness Speaker Speedwell Sphinx Splendid Sprite Stag Standard Stately Success Sunlight Sunbeam Superb Swan Sweepstake Swift Swiftsure
	Terrible Terror Thunderer Tiger Tremendous Trident Trinity Triumph Trusty
	Unicorn Union Unity Valiant Vanguard Venerable Vestal Veteran Victor Vindictive Virtue Violet
	Warrior Warspite "Young Prince" Zealous

	Survey Surveillance Cow Ox Bird Hawk Sovereign Emperor Count Lord Baron

	Knight Paladin Dragonslayer Cleric Rogue Fighter Ranger Sorcerer Wizard Warlock Monk Druid
	Strength Dexterity Constitution Intelligence Wisdom Charisma
}

army_names = {
	"Emperor's Legion" "Dragon Legion" "Beastbane Legioon" "$PROVINCE$ Legion"
}

fleet_names = {
	"Imperial Fleet" "Giant's Grave Squadron" "Bay of Chills Squadron" "Dragon Squadron" "Blue Squadron" "White Squadron" "Flame Squadron" "Sapphire Squadron" "Wall Squadron"
}