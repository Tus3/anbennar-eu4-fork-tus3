#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 57  81  87 }

revolutionary_colors = { 57  81  87 }

historical_idea_groups = {
	economic_ideas
	offensive_ideas
	exploration_ideas
	defensive_ideas	
	administrative_ideas	
	maritime_ideas
	quality_ideas
	innovativeness_ideas
}

historical_units = {
	cannorian_1_medieval_infantry
	cannorian_1_medieval_knights
	cannorian_5_men_at_arms
	cannorian_9_pike_and_shot
	cannorian_10_gun_knights
	cannorian_12_thorn_formation
	cannorian_14_outriders
	cannorian_15_volley_infantry
	cannorian_18_reformed_outriders
	cannorian_19_thorn_formation2
	cannorian_23_line_infantry
	cannorian_26_carabiners
	cannorian_26_rosecoat_infantry
	cannorian_28_impulse_infantry
	cannorian_30_cuirassier
	cannorian_30_drill_infantry
}

monarch_names = {

	#Generic Alenic Names
	"Acromar #0" = 1
	"Adelar #0" = 1
	"Alain #0" = 1
	"Alen #0" = 1
	"Arnold #0" = 1
	"Camir #0" = 1
	"Camor #0" = 1
	"Canrec #0" = 1
	"Carlan #0" = 1
	"Celgal #0" = 1
	"Ciramod #0" = 1
	"Clarimond #0" = 1
	"Clothar #0" = 1
	"Colyn #0" = 1
	"Coreg #0" = 1
	"Crovan #0" = 1
	"Crovis #0" = 1
	"Corac #0" = 1
	"Corric #0" = 1
	"Dalyon #0" = 1
	"Delia #0" = 1
	"Delian #0" = 1
	"Devac #0" = 1
	"Devan #0" = 1
	"Dustyn #0" = 1
	"Elecast #0" = 1
	"Edmund #0" = 1
	"Frederic #0" = 1
	"Godrac #0" = 1
	"Godric #0" = 1
	"Godryc #0" = 1
	"Godwin #0" = 1
	"Gracos #0" = 1
	"Henric #0" = 1
	"Humac #0" = 1
	"Humban #0" = 1
	"Humbar #0" = 1
	"Humbert #0" = 1
	"Jacob #0" = 1
	"James #0" = 1
	"Lain #0" = 1
	"Lan #0" = 1
	"Madalac #0" = 1
	"Marcan #0" = 1
	"Ottrac #0" = 1
	"Ottran #0" = 1
	"Petran #0" = 1
	"Peyter #0" = 1
	"Rabac #0" = 1
	"Rabard #0" = 1
	"Rycan #0" = 1
	"Rican #0" = 1
	"Ricard #0" = 1
	"Rogec #0" = 1
	"Roger #0" = 1
	"Stovac #0" = 1
	"Stovan #0" = 1
	"Teagan #0" = 1
	"Tomac #0" = 1
	"Toman #0" = 1
	"Tomar #0" = 1
	"Tormac #0" = 1
	"Ulric #0" = 1
	"Venac #0" = 1
	"Vencan #0" = 1
	"Walter #0" = 1
	"Welyam #0" = 1
	"Wystan #0" = 1
	"Bellac #0" = 1
	

	"Adela #0" = -10
	"Alice #0" = -10
	"Anna #0" = -10
	"Anne #0" = -10
	"Auci #0" = -10
	"Bella #0" = -10
	"Catherine #0" = -10
	"Clarimonde #0" = -10
	"Clarya #0" = -10
	"Clothilde #0" = -10
	"Constance #0" = -10
	"Cora #0" = -10
	"Coralinde #0" = -10
	"Dalya #0" = -10
	"Edith #0" = -10
	"Eleanor #0" = -10
	"Elenor #0" = -10
	"Emma #0" = -10
	"Etta #0" = -10
	"Humba #0" = -10
	"Lisban #0" = -10
	"Lisbet #0" = -10
	"Madala #0" = -10
	"Magda #0" = -10
	"Matilda #0" = -10
	"Robyn #0" = -10
}

leader_names = {
	#Country Specific
	"of Vertesk" "of Dragondock" "of Black Tower" "of the Floodmarches" "of Highharbour"
	
	#Province Neighbours
	"of Tenbury" "of Greenley" "of Fluddhill" "of Alenath" "of Aramar" "of Croth�n" "of Crownsway" "of Wrothcross" "of Crodam" "of Ginnfield" "of Elwick" "of Talonmount" "of Gaweton" "of Drakesford"
	"of Westmoors" "of Moorton"
	
	#Country Neighbours
	"of Wex" "of Derwing" "of Arbaran" "of Gawed" "of Anbennc�st" "of Damescrown" "of Uelaire" "of Beepeck" "of Exwes" "of Ryalanar" "of Lorent" "of Deranne"
	
	#Geographic Neighbours
	"of Esmaria" "of the Borders" "of the Dameshead" "of Damesneck" "of Lencenor" 
	
	#Noble Families
	"of Vanbury" Gerwick Baldfather Greenley Tenbury 
	Cottersea Cockerwall Beron Brontay Leapdon
	Silcalas Silurion Silgarion Konwell "s�l Themarenn" "s�l Bennon"
	
	
	#Generic English names and occupational names
	Smith Youngheart Adamson Henryson Donaldson Cook Baker Rider Lampman Fisher Fischer Tanner Miller Banks Taylor Cobble Jones Davies Williams Roberts White Black Grey Gray Green Red Blue Edwards Wood Woods Clarke Davis Nicholson
	Priestley Campman Wright Parker Stewart Berry Porter Shepherd Freeman Potter Pitcher Alderman Arkwright Barber Baker Bailey Bender Bowyer Chapman Chandler Cheeseman Cook Cooper Cowell Crocker Cutler Earl Farmer 
	
	#Titles
	"'the Tall'" "'the Younger'" "'the Clever'" "'the Vigilant'" "'the Stern'" "'the Mighty'" "'Halfblood'" "'the Mudblooded'" "'the Kind'" "'the Able'" "'the Good'" "'the Fat'"
}

ship_names = {
	#Generic Cannorian
	Adamant Advantage Adventure Advice Answer Ardent Armada Arrogant Assistance Association Assurance Atlas Audacious 
	Bear Beaver "Black Galley" "Black Pinnace" "Black Prince" "Black Blood" Captain Centurion Coronation Courage Crocodile Crown
	Daisy Defence Defiance Devastation Diamond Director Dolphin Dragon Drake Dreadnaught Duke Duchess Eagle Elephant Excellent Exchange Expedition Experiment
	Falcon Fame Favourite Fellowship Fleetwood Flight Flirt Formidable Forrester Fortitude Fortune Gillyflower Globe "Golden Horse" "Golden Lion" "Golden Phoenix" Goliath Goodgrace Governor
	"Grand Mistress" "Great Bark" "Great Charity" "Great Galley" "Great Gift"  "Great Pinnace" "Great Zebra" "Green Dragon" Greyhound Griffin Guide "Half Moon" Hare Harpy Hawke Hazardous Heart
	Hero Hope "Hope Bark" "Hopeful Adventure" Humble Hunter Illustrious Impregnable Increase Indefatigable Inflexible Intrepid Invincible "Less Bark" "Less Pinnace" "Less Zebra"
	Lively Lizard Lion Magnanime Magnificent Majestic Makeshift Medusa Minotaur Moderate Monarch Moon Moor "New Bark" Ocean 
	Pansy Panther Parrot Porcupine Powerful President Prince "Prince Consort" "Prince Royal"
	Redoubtable Reformation Regent Renown Repulse Research Reserve Resistance Resolution Restoration Revenge 
	Salamander Sandwich Sapphire Satisfaction Seahorse Search Sheerness Speaker Speedwell Sphinx Splendid Sprite Stag Standard Stately Success Sunlight Sunbeam Superb Swan Sweepstake Swift Swiftsure
	Terrible Terror Thunderer Tiger Tremendous Trident Trinity Triumph Trusty
	Unicorn Union Unity Valiant Vanguard Venerable Vestal Veteran Victor Vindictive Virtue Violet
	Warrior Warspite "Young Prince" Zealous

	Survey Surveillance Cow Ox Bird Hawk Sovereign Emperor Count Lord Baron

	Knight Paladin Dragonslayer Cleric Rogue Fighter Ranger Sorcerer Wizard Warlock Monk Druid
	Strength Dexterity Constitution Intelligence Wisdom Charisma
	
	#Elven
	Lunetine Agraseina 
	
	#Regent Court Deities
	Castellos Dame "The Dame" Halanna Ysh Yshtralania Agrados Adean Esmaryal Ryala Edronias Falah Nerat Ara Minara Munas Moonsinger Nathalyne Begga Corin Balgar
	Uelos Drax'os
}

army_names = {
	"Vertesker Army" "Army of the Marches" "Army of $PROVINCE$"
}

fleet_names = {
	"Vertesker Fleet" "Damescrown Squadron" "Dameshead Squadron" "Damesneck Squadron" "Dragondock Squadron" "White Squadron" "Wing Squadron"
}