#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 143  120  28 }

revolutionary_colors = { 143  120  28 }

historical_idea_groups = {
	expansion_ideas
	exploration_ideas
	quantity_ideas
	diplomatic_ideas	
	offensive_ideas
	defensive_ideas
	economic_ideas	
	innovativeness_ideas
}

historical_units = {
	western_medieval_infantry
	chevauchee
	western_men_at_arms
	swiss_landsknechten
	dutch_maurician
	french_caracolle
	anglofrench_line
	french_dragoon
	french_bluecoat
	french_cuirassier
	french_impulse
	mixed_order_infantry
	open_order_cavalry
	napoleonic_square
	napoleonic_lancers
}

monarch_names = {

	#Generic Dwarf Names
	
	"Adar #0" = 1
	"Adranbur #0" = 1
	"Ad�n #0" = 1
	"Balgar #0" = 1
	"Balgor #0" = 1
	"Balin #0" = 1
	"Bardin #0" = 1
	"Bardur #0" = 1
	"Beld�n #0" = 1
	"Belgar #0" = 1
	"Bofur #0" = 1
	"Bolin #0" = 1
	"Bombur #0" = 1
	"Borin #0" = 1
	"Brann #0" = 1
	"Brok #0" = 1
	"Bruenor #0" = 1
	"Calin #0" = 1
	"C�n #0" = 1
	"Dagran #0" = 1
	"Dain #0" = 1
	"Dalbaran #0" = 1
	"Dalmund #0" = 1
	"Dolin #0" = 1
	"Dorin #0" = 1
	"Dorri #0" = 1
	"Durb�r #0" = 1
	"Durin #0" = 1
	"Dwalin #0" = 1
	"Dwarin #0" = 1
	"Ed�n #0" = 1
	"Falstad #0" = 1
	"Far�n #0" = 1
	"Fimbur #0" = 1
	"Flondi #0" = 1
	"Foldan #0" = 1
	"Gorar #0" = 1
	"Goren #0" = 1
	"Gotrek #0" = 1
	"Grim #0" = 1
	"Grimmar #0" = 1
	"Grimmun #0" = 1
	"Gromar #0" = 1
	"Gronmar #0" = 1
	"Gr�aim #0" = 1
	"Gr�ar #0" = 1
	"Gr�n #0" = 1
	"Gundobad #0" = 1
	"Gundobar #0" = 1
	"Gundobin #0" = 1
	"Gundomar #0" = 1
	"Hard�n #0" = 1
	"Harl�n #0" = 1
	"Harr�k #0" = 1
	"Hjal #0" = 1
	"Hjalmar #0" = 1
	"Karrom #0" = 1
	"Kazad�r #0" = 1
	"Khadan #0" = 1
	"Kildan #0" = 1
	"Kildar #0" = 1
	"Kild�m #0" = 1
	"K�rg�m #0" = 1
	"Lod�n #0" = 1
	"Lorek #0" = 1
	"Lorimbur #0" = 1
	"Madar�n #0" = 1
	"Magar #0" = 1
	"Magni #0" = 1
	"Magnus #0" = 1
	"Mag�n #0" = 1
	"Morzad #0" = 1
	"Muradin #0" = 1
	"Nali #0" = 1
	"Nalin #0" = 1
	"Norb�n #0" = 1
	"Norb�r #0" = 1
	"Nori #0" = 1
	"Norimbur #0" = 1
	"Norin #0" = 1
	"Odar #0" = 1
	"Odur #0" = 1
	"Od�n #0" = 1
	"Okar #0" = 1
	"Olin #0" = 1
	"Orim #0" = 1
	"Orin #0" = 1
	"Ragun #0" = 1
	"Ragund #0" = 1
	"Runmar #0" = 1
	"R�nim�r #0" = 1
	"Sevrund #0" = 1
	"Simb�r #0" = 1
	"Simur #0" = 1
	"Sim�n #0" = 1
	"Sindri #0" = 1
	"Snorri #0" = 1
	"Storin #0" = 1
	"Storlin #0" = 1
	"Storum #0" = 1
	"Thargas #0" = 1
	"Thargor #0" = 1
	"Thinbur #0" = 1
	"Thindobar #0" = 1
	"Thindri #0" = 1
	"Thingrim #0" = 1
	"Thinobad #0" = 1
	"Thorar #0" = 1
	"Thorgrim #0" = 1
	"Thorin #0" = 1
	"Thrain #0" = 1
	"Thrair #0" = 1
	"Thror #0" = 1
	"Thr�n #0" = 1
	"Thr�r #0" = 1
	"Thudri #0" = 1
	"Tungdil #0" = 1
	"Ungrim #0" = 1
	"Urist #0" = 1
	"Ur�r #0" = 1
	
	"Adala #0" = -10
	"Aira #0" = -10
	"Anvi #0" = -10
	"Auci #0" = -10
	"Balga #0" = -10
	"Barila #0" = -10
	"Barra #0" = -10
	"Belga #0" = -10
	"Belgia #0" = -10
	"Bella #0" = -10
	"Branma #0" = -10
	"Brenla #0" = -10
	"Brenleil #0" = -10
	"Brenlel #0" = -10
	"Brina #0" = -10
	"Broga #0" = -10
	"Bronis #0" = -10
	"Cala #0" = -10
	"Dalina #0" = -10
	"Dwalra #0" = -10
	"Edeth #0" = -10
	"Etta #0" = -10
	"Grelba #0" = -10
	"Grimma #0" = -10
	"Groria #0" = -10
	"Gruia #0" = -10
	"Isondi #0" = -10
	"Lahgi #0" = -10
	"Lisban #0" = -10
	"Magda #0" = -10
	"Mistri #0" = -10
	"Moira #0" = -10
	"Mori #0" = -10
	"Moria #0" = -10
	"Naim #0" = -10
	"Ovondi #0" = -10
	"Ronla #0" = -10
	"Sivondi #0" = -10
	"Tharkuni #0" = -10
	"Thohga #0" = -10
	"Ungi #0" = -10
}

leader_names = {

	Battlehammer Axehammer Ettinkiller Minesmiter Minesmith Hammersmith Forgehammer Bloodsteel Earthfist Bronzebeard Goldbeard Stonebeard Hammerbeard Frostbeard Whitebeard
	Breakbeard Ringbeard Gembeard Ogreslayer Orcslayer Spiderslayer Goblinslayer Graybeard Greybeard Redbeard Warhorn Gemhorn Longbeard Truebellow Blackbellow Strongbellow
	Shalesmith Shaleaxe Shalehammer Shaleforger Shalebeard Heavyaxe Skullaxe Gemcraver Goodanvil Blackanvil Shaleanvil Forgeanvil Axeanvil Trueanvil Bloodanvil 
	Thunderfist Thunderhammer Thunderaxe Stormfist Stormhammer Stormaxe Stormanvil Stormbeard Stromanvil Thundersteel Stormsteel Elderbeard Oldbeard Copperbeard Gritaxe
	Flameforge Flameaxe Flamehammer Flamefist Hammerkeeper Stormkeeper Beardfist Hornfist Battlebeard Hardaxe Hardhammer Hammerhand Forgebar Ironbar Goldbar Bronzebar Steelbar
	Ironstar Ironaxe Ironsteel Ironhammer Ironfist Ironhelm
	
}
ship_names = {
	#Generic Cannorian
	Adamant Advantage Adventure Advice Answer Ardent Armada Arrogant Assistance Association Assurance Atlas Audacious 
	Bear Beaver "Black Galley" "Black Pinnace" "Black Prince" "Black Blood" Captain Centurion Coronation Courage Crocodile Crown
	Daisy Defence Defiance Devastation Diamond Director Dolphin Dragon Drake Dreadnaught Duke Duchess Eagle Elephant Excellent Exchange Expedition Experiment
	Falcon Fame Favourite Fellowship Fleetwood Flight Flirt Formidable Forrester Fortitude Fortune Gillyflower Globe "Golden Horse" "Golden Lion" "Golden Phoenix" Goliath Goodgrace Governor
	"Grand Mistress" "Great Bark" "Great Charity" "Great Galley" "Great Gift"  "Great Pinnace" "Great Zebra" "Green Dragon" Greyhound Griffin Guide "Half Moon" Hare Harpy Hawke Hazardous Heart
	Hero Hope "Hope Bark" "Hopeful Adventure" Humble Hunter Illustrious Impregnable Increase Indefatigable Inflexible Intrepid Invincible "Less Bark" "Less Pinnace" "Less Zebra"
	Lively Lizard Lion Magnanime Magnificent Majestic Makeshift Medusa Minotaur Moderate Monarch Moon Moor "New Bark" Ocean 
	Pansy Panther Parrot Porcupine Powerful President Prince "Prince Consort" "Prince Royal"
	Redoubtable Reformation Regent Renown Repulse Research Reserve Resistance Resolution Restoration Revenge 
	Salamander Sandwich Sapphire Satisfaction Seahorse Search Sheerness Speaker Speedwell Sphinx Splendid Sprite Stag Standard Stately Success Sunlight Sunbeam Superb Swan Sweepstake Swift Swiftsure
	Terrible Terror Thunderer Tiger Tremendous Trident Trinity Triumph Trusty
	Unicorn Union Unity Valiant Vanguard Venerable Vestal Veteran Victor Vindictive Virtue Violet
	Warrior Warspite "Young Prince" Zealous

	Survey Surveillance Cow Ox Bird Hawk Sovereign Emperor Count Lord Baron

	Knight Paladin Dragonslayer Cleric Rogue Fighter Ranger Sorcerer Wizard Warlock Monk Druid
	Strength Dexterity Constitution Intelligence Wisdom Charisma
	
	#Elven
	Lunetine Agraseina Estaforra
	
	#Regent Court Deities
	Castellos Dame "The Dame" Halanna Ysh Yshtralania Agradls Adean Esmaryal Ryala Edronias Falah Nerat Ara Minara Munas Moonsinger Nathalyne Begga Corin Balgar
	Uelos Drax'os
}

army_names = {
	"Asra Expedition" "Asra Army" "Army of $PROVINCE$"
}

fleet_names = {
	"Asra Expedition Fleet" "Bronze Squadron" "Steel Squadron" "Iron Squadron" "Copper Squadron" "Gold Squadron"
}