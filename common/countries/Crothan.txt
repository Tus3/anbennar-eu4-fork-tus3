#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 38  156  162 }

revolutionary_colors = { 38  156  162 }

historical_idea_groups = {
	exploration_ideas
	maritime_ideas
	economic_ideas
	offensive_ideas
	expansion_ideas
	quality_ideas	
	administrative_ideas	
	influence_ideas
}

historical_units = {
	cannorian_1_medieval_infantry
	cannorian_1_medieval_knights
	cannorian_5_men_at_arms
	cannorian_9_pike_and_shot
	cannorian_10_gun_knights
	cannorian_12_thorn_formation
	cannorian_14_outriders
	cannorian_15_volley_infantry
	cannorian_18_reformed_outriders
	cannorian_19_thorn_formation2
	cannorian_23_line_infantry
	cannorian_26_carabiners
	cannorian_26_rosecoat_infantry
	cannorian_28_impulse_infantry
	cannorian_30_cuirassier
	cannorian_30_drill_infantry
}

monarch_names = {

	#Generic Anbennarian Name List
	"Adelar #0" = 1
	"Ademar #0" = 1
	"Adran #0" = 1
	"Adrian #0" = 1
	"Adrien #0" = 1
	"Alain #0" = 1
	"Alec #0" = 1
	"Albert #0" = 1
	"Aldred #0" = 1
	"Alfons #0" = 1
	"Alfred #0" = 1
	"Alos #0" = 1
	"Alvar #0" = 1
	"Andrel #0" = 1
	"Ardan #0" = 1
	"Ardor #0" = 1
	"Aril #0" = 1
	"Arman #0" = 1
	"Artorian #0" = 1
	"Artur #0" = 1
	"Aucan #0" = 1
	"Austyn #0" = 1
	"Avery #0" = 1
	"Awen #0" = 1
	"Borian #0" = 1
	"Brandon #0" = 1
	"Brayan #0" = 1
	"Brayden #0" = 1
	"Calas #0" = 1
	"Caylen #0" = 1
	"Cast�n #0" = 1
	"Cast�n #0" = 1
	"Cecill #0" = 1
	"Corin #0" = 1
	"Cristof #0" = 1
	"Daran #0" = 1
	"Darran #0" = 1
	"Davan #0" = 1
	"Denar #0" = 1
	"Dominic #0" = 1
	"Dustin #0" = 1
	"Edmund #0" = 1
	"Elran #0" = 1
	"Emil #0" = 1
	"Erel #0" = 1
	"Eren #0" = 1
	"Erlan #0" = 1
	"Evin #0" = 1
	"Frederic #0" = 1
	"Galin #0" = 1
	"Gelman #0" = 1
	"Kalas #0" = 1
	"Laurens #0" = 1
	"Lucian #0" = 1
	"Luciana #0" = 1
	"Marion #0" = 1
	"Maurise #0" = 1
	"Nara #0" = 1
	"Olor #0" = 1
	"Ot� #0" = 1
	"Re�n #0" = 1
	"Riann�n #0" = 1
	"Ricain #0" = 1
	"Ri�n #0" = 1
	"Robin #0" = 1
	"Rogier #0" = 1
	"Sandur #0" = 1
	"Taelar #0" = 1
	"Teagan #0" = 1
	"Thal #0" = 1
	"Thiren #0" = 1
	"Tom�s #0" = 1
	"Trian #0" = 1
	"Tristan #0" = 1
	"Trystan #0" = 1
	"Valen #0" = 1
	"Valeran #0" = 1
	"Varian #0" = 1
	"Varil #0" = 1
	"Varilor #0" = 1
	"Varion #0" = 1
	"Vernell #0" = 1
	"Vincen #0" = 1
	"Willam #0" = 1
	
	"Adeline #0" = -10
	"Adra #0" = -10
	"Alara #0" = -10
	"Aldresia #0" = -10
	"Alina #0" = -10
	"Alisanne #0" = -10
	"Amarien #0" = -10
	"Amina #0" = -10
	"Arabella #0" = -10
	"Aria #0" = -10
	"Athana #0" = -10
	"Adela #0" = -10
	"Aucanna #0" = -10
	"Bella #0" = -10
	"Calassa #0" = -10
	"Cast�nnia #0" = -10
	"Cast�na #0" = -10
	"Cecille #0" = -10
	"Cela #0" = -10
	"Celadora #0" = -10
	"Clarimonde #0" = -10
	"Constance #0" = -10
	"Cora #0" = -10
	"Coraline #0" = -10
	"Corina #0" = -10
	"Eil�s #0" = -10
	"Eil�sabet #0" = -10
	"El�anore #0" = -10
	"Emil�e #0" = -10
	"Erela #0" = -10
	"Erella #0" = -10
	"Galina #0" = -10
	"Galinda #0" = -10
	"Gis�le #0" = -10
	"Isabel #0" = -10
	"Isabella #0" = -10
	"Isobel #0" = -10
	"Kerstin #0" = -10
	"Laurenne #0" = -10
	"Lianne #0" = -10
	"Madal�in #0" = -10
	"Margery #0" = -10
	"Maria #0" = -10
	"Mariana #0" = -10
	"Marianna #0" = -10
	"Marianne #0" = -10
	"Marien #0" = -10
	"Marina #0" = -10
	"Re�nna #0" = -10
	"Sofia #0" = -10
	"Sofie #0" = -10
	"Sybille #0" = -10
	"Thalia #0" = -10
	"Valence #0" = -10
	"Varina #0" = -10
	"Varinna #0" = -10
	"Willamina #0" = -10
	"Lisolette #0" = -10
}

leader_names = {
	d'Ambleteuse d'Aulnay
	"de Beauharnais" "de Bercy" "de B�thune" "de Boissieu" "de Bonnefoy" "de Bonneuil" "du Bosquet" "de la Bretonni�re" "de Broglie"
	"de Castaing" "de Caumartin" "de Chambly" "de Chambronne" "de Champmartin" "de Chevigny" "des Coulons" "de Cr�vec�ur" "de Crussol"
	"de Dampierre" Desgouttes Duquesne
	"des �cures" d'Entraigues d'Estr�es
	"du Fournay"
	"de Galard"
	"des Herbiers"
	"de La Barthe" "de La Ferrandie" "de La Galissoni�re" "de La Mothe" "de La Motte d'Airan" "de La Porte" "Le Tellier" "de Lh�ry" "de Luynes"
	"de Marguerye" "de Maurepas"
	"de Neufch�tel"
	"d'Ornano"
	"de Pellefort"
	"de Richemont" "de Rochechouart" "de Rochemaure" "de Rieu"
	"de Saint-Chamond" "de Saint-Esprit" "de Saint-Germain" "de Saint-Omer" "de Siorac"
	"de Tocqueville" "de La Tour d'Auvergne" "de Tr�ville"
	"d'Usson"
	"du Valentinois" "de Vergennes" "de Vibien" "de Vigny" "de Villeneuve"
}

ship_names = {
	"Amiral de Biscaye" "Amiral de Galice" "Arc-en-Ciel" "Belle Poule" "Bien-Aim�" "Charles II" "Cheval Marin" "Commerce de Bordeaux" "Commerce de Marseille"
	"Commerce de Paris" "Commerce du Lyon" "Couronne Ottomane" "Croissant d'Afrique" "Dauphin Royal" "Deux Fr�res" "Don de Dieu" "Dragon d'Or" "Duc d'Angoul�me"
	"Duc d'Aquitaine" "Duc de Berry" "Duc de Bourgogne" "�sperance d'Angleterre" "�sperance de Dieu" "�sperance en Dieu" "�tats de Bourgogne" "�toile de Diane"
	"Faucon Anglais" "Fleur de Lys" "Fr�gate Royale" "Galion de Guise" "Grand Anglais" "Grand Chalain" "Grand Danois" "Grand Henry" "Grand Saint Louis"
	"Grand Vainqueur" "Grand Vasseau du Roi" "Grande Galion de Malte" "Grande Nef d'�cosse" "Grande Saint Jean" "�le de France" "Le Beau Parterre" "Lion Couronn�"
	"Lion d'Or" "Madeleine de Brest" "Marguerite du Ponant" "Marie la Cordeli�re" "Marie-Elisabeth" "Mont Saint-Bernard" "Navire-du-Roi" "Noire Gal�re"
	"Notre Dame de la Victoire" "Notre Dame des Martyres" "Notre Dame du Peuple" "Notre Dame" "Ours Blanc" "Ours d'Or" "Petit Saint Jean" "Petit Saint Louis"
	"Prince Henri" "Quatre Fr�res" "Roi de Rome" "Royal Duc" "Royal Hollandais" "Royal Italien" "Royal Louis" "Royale Th�r�se" "Saint Andr�" "Saint Antoine de G�nes"
	"Saint Antoine" "Saint Augustin" "Saint Basile" "Saint Catherine" "Saint Charles" "Saint Cosme" "Saint Dominique" "Saint Edm�" "Saint Esprit" "Saint Fran�ois"
	"Saint Georges de Londres" "Saint Ignace" "Saint Jacques du Portugal" "Saint Jacques" "Saint Jean de Bordeaux" "Saint Jean de Hollande" "Saint Jean" "Saint Joseph"
	"Saint Louis de Brest" "Saint Louis de Nevers" "Saint Louis de Saint-Malo" "Saint Louis" "Saint Michel" "Saint Paul" "Saint Philippe" "Saint Pierre" "Saint S�bastien"
	"Saint Thomas d'Aquin" "Saint Vincent" "Sainte Croix" "Sainte Genevi�ve" "Sainte Marie" "Saint-Malo" "Saint-Sacrement" "Sans Pareil" "Soleil d'Afrique"
	"Soleil d'Alger" "Soleil Royal" "Trois Fanaux" "Trois Rois" "Vaisseau de Torais" "Vaisseau du Roi" "Ville-de-Marseille" "Ville-de-Paris" "Ville-de-Rouen"
	Achille Actif Actionnaire Adela�de Admirable Adroit Africain Agamemnon Aigle Aigrette Aimable Ajax Alcide Alcyon Alexandre Alliance Alsace Altier Amarante
	Amazone Ambitieux America Amiral Amphion Amphitrite Andromaque Anglais Anna Annibal Anonyme Anversois Apollon Aquilon Arcole Ardent Ar�thuse Argonaute Art�sien
	Asie Assur� Astr�e Astrolabe Ath�nien Atlante Audacieux Auguste Aurore Avenant Aventurier Badine Banel Basque Beaufort Belliqueux Bellone Bienfaisant Bizarre Bon
	Bonne Bordelais Bor�e Boudeuse Bouffonne Bourbon Boussole Brave Br�l� Bretagne Breton Brillant Brutal Bucentaure Calypso Capable Capricieux Caraquon Cardinal
	Caribou Cassard Castiglione Catholique Caton Censeur Centaure C�sar Chalain Charente Charlemagne Charmante Chasseur Choquante Christine Citoyen Cl�op�tre Clorinde
	Colosse Com�te Comte Concorde Conqu�rant Consolante Content Coq Corail Courageux Couronne Courtisan Croissant Curieux Cygne Dalmate Dans� Dantzig Dauphin D�daigneuse
	D�fenseur Dego Desaix Destin Diad�me Diamant Dictateur Dragon Dryade Dubois Duc Duguesclin Dunkerquois Duperre Duquesne Dur �clatant �cueil �cureuil �glise Elbeuf
	Embuscade �merillon �minent Empereur Emporte Engageante Enjouree Entendu Entreprenant �ole �sperance Europe �veill� Excellent F�cheux Fantasque Faucon Favori F�e
	F�licit� Fendant Ferroni�re Fid�le Fier Fiesque Flamand Fleuron Florissant Formidable Fort Fortune Foudroyant Fougueux Fran�ais Fr�d�ric Friponne Fulminant Gaillard
	Galant Galath�e Gaulois Gazelle G�n�reux G�nois Gentille Gerze Gloire Gracieuse Grand Griffon Guerrier Guirlande Hardi Hasardeux Havre H�b� Henri Hercule Hermione
	H�ro�ne H�ros Heureux Hippopotame Hirondelle Hymen Inconstante Indien Indomptable Infante Inflexible Intr�pide Invincible Iphigh�nie Isabelle Jason Jemmapes Jeux
	Joli Jules Junon Jupiter Juste Languedoc Laurier L�ger L�opard Licorne Ligourois Lion Lionne Lune Lutine Lys Madame Madeleine Magicienne Magnanime Magnifique
	Maistresse Majestueux Maquedo Marabout Marguerite Marin Marquise Mars Maure Maurepas Mazarin M�duse Merc�ur Mercure Merveilleux Mignonne Minotaure Mod�r� Monarque
	Montebello Montenotte Montmorency Montolieu Mutine Nassau Navarrais Neptune N�r�ide Nieuport Normand Oc�an Oiseau Opini�tre Orgueilleux Orient Oriflamme Orion Orph�e
	Ours Pacificateur Palmier Parfait Paris Patriote P�gase P�lican P�licorne Perle Pers�e Pers�v�rante Pl�iade Poli Polonais Polyph�me Pomp�e Pompeux Pourvoyeuse Prince
	Princesse Prompt Pros�lyte Prot�e Proven�al Prudent Pucelle Puissant Pyrrhus Railleuse Raisonnable Redoutable R�fl�chi R�g�n�rateur Regina Regulus Reine Renomm�e
	R�sistance Robuste Rochefort Roland Rouen Royal Rubis Sage Sagittaire Salamandre Sceptre Scipion S�duisant Seine S�millante S�rieux Sir�ne Soleil Solide Sophie
	Sorlingue Sourdis Souverain Sphinx Subtile Suffisant Suffren Superbe Surveillante Suzanne Sylvie T�m�raire Temp�te Terpsichore Terreur Terrible Th�mistocle Th�r�se
	Th�s�e Th�tis Tigre Tonnant Topaze Toulon Tourbillon Tourville Trajan Trident Triomphe Triton Trocadero Trompeuse Uranie Utile Vaillant Vainqueur Valeur Vend�me
	Vengeur V�nus Vertu Vestale V�t�ran Victoire Victorieux Vierge Volage Volontaire Wallon Z�landais Z�l� Zodiaque
}

army_names = {
	"Arm�e royale" "Arm�e des Flandres" "Arm�e du Nord" "Arm�e de l'Est" "Arm�e du Jura" "Arm�e du Lyonnais" "Arm�e des Alpes"
	"Arm�e du Midi" "Arm�e des Pyr�n�es" "Arm�e de l'Atlantique" "Arm�e de Bretagne" "Arm�e d'Italie" "Arm�e de $PROVINCE$"
}

fleet_names = {
	"Escadre de la Manche" "Escadre de l'Atlantique" "Escadre de M�diterran�e" "Escadre de Gascogne" "Escadre Bleue" "Escadre Rouge" "Escadre Blanche"
}