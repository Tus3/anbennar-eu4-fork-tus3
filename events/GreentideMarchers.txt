
namespace = marcher

#New Country Formed - default
country_event = {
	id = marcher.1
	title = marcher.1.t
	desc = marcher.1.d
	picture = STREET_SPEECH_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		#government_type = adventurer
		#government_rank = 2
	}
	
	option = {		# Despotic Monarchy
		name = "marcher.1.a"
		ai_chance = { 
			factor = 60
			modifier = {
				factor = 10
				tag = B07 #Sons of Dameria
			}
			modifier = {
				factor = 2
				faction_in_power = adv_marchers
			}
		}	
		change_government = despotic_monarchy
		
		#Dynasty name triggers for each country
		hidden_effect = {
			country_event = { id = flavor_luciande.1 days = 1 }
			country_event = { id = flavor_ancardia.1 days = 1 }
			country_event = { id = flavor_rogieria.1 days = 1 }
			country_event = { id = flavor_elikhand.1 days = 1 }
			country_event = { id = flavor_wyvernheart.1 days = 1 }
			country_event = { id = flavor_alenor.1 days = 1 }
			country_event = { id = flavor_stalbor.1 days = 1 }
			country_event = { id = flavor_ravenmarch.1 days = 1 }
			country_event = { id = flavor_araionn.1 days = 1 }
			country_event = { id = flavor_newshire.1 days = 1 }
			country_event = { id = flavor_estaire.1 days = 1 }
			country_event = { id = flavor_anbenland.1 days = 1 }
			country_event = { id = flavor_nurcestir.1 days = 1 }
			country_event = { id = flavor_esthil.1 days = 1 }
		}
	}
	option = {		# Oligarchic Republic
		name = "marcher.1.b"
		ai_chance = { 
			factor = 30
			modifier = {
				factor = 1.5
				faction_in_power = adv_pioneers
			}
			modifier = {
				factor = 2	#Having a poor ruler makes it likely they want an elective country
				OR = {
					NOT = { adm = 3 }
					NOT = { dip = 3 }
					NOT = { mil = 3 }
				}
			}
		}
		change_government = oligarchic_republic
	}
	option = {		# Merchant Republic
		name = "marcher.1.c"
		ai_chance = { 
			factor = 10 
			modifier = {
				factor = 4
				faction_in_power = adv_fortune_seekers
			}
			modifier = {
				factor = 8
				OR = {
					tag = B09 #House of Riches
				}
			}
		}
		change_government = merchant_republic
	}
	
	#Optional
	
	#Damerian Monarchy
	#Magocracy
}