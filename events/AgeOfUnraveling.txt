namespace = unraveling

# Rediscovery of Aelantir!
country_event = {
	id = unraveling.0
	title = unraveling.0.t
	desc = unraveling.0.d
	picture = COLONIZATION_eventPicture
	
	
	fire_only_once = yes
	
	trigger = {
		NOT = { has_global_flag = new_world_discovered }
		# OR = {
			# AND = {
				# tag = CAS
				# NOT = { exists = SPA }
			# }
			# tag = SPA
		# }
		OR = {
			north_america = {
				has_discovered = ROOT
			}
			south_america = {
				has_discovered = ROOT
			}
			new_world = {
				has_discovered = ROOT
			}
		}
	}

	mean_time_to_happen = {
		months = 1
	}
	
	immediate = {
		set_global_flag = new_world_discovered
	}

	option = {		# Now so have we!
		name = "flavor_spa.EVTOPTA1005"
		add_prestige = 50
		add_innovativeness_small_effect = yes
	}
}

# Castellos is Dead
country_event = {
	id = unraveling.1
	title = unraveling.1.t
	desc = unraveling.1.d
	picture = COMET_SIGHTED_eventPicture
	
	trigger = {
		has_global_flag = new_world_discovered
		is_year = 1510	
		
		any_province = {
			#limit = {
				OR = {
					continent = north_america
					continent = south_america
				}
		#	}
			owner = {
				capital_scope = { 
					NOT = { 
						continent = north_america
						continent = south_america
						continent = new_world
					}
				}
			}
		}
	}
	
	mean_time_to_happen = {
		months = 240
		
		# modifier = {
			# factor = 0.4
			# OR = {
				# tag = B02 #Corintar
			# }
		# }
	}

	option = {
		name = "unraveling.1.a"	#What other secrets does Aelantir hold? The Age of Unraveling has begun.
		add_stability = -3
		
		hidden_effect = {
			country_event = { id = unraveling.2 days = 1 }
		}
	}
}

country_event = {
	id = unraveling.2
	title = unraveling.2.t
	desc = unraveling.2.d
	picture = COMET_SIGHTED_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	fire_only_once = yes


	option = {
		name = "unraveling.1.a"	#What other secrets does Aelantir hold? The Age of Unraveling has begun.
		set_global_flag = "castellos_is_dead" 
	}
}